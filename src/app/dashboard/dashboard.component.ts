import { Component, OnInit } from "@angular/core";
import { AdminService } from "../providers/admin.service";
@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"]
})
export class DashboardComponent implements OnInit {
  teacherCounter: number;
  data: any;
  options: any;
  constructor(public adminService: AdminService) {}

  ngOnInit() {
    this.getTeacherCount();

    this.options = {
      title: {
        display: true,
        text: "Registration Graph",
        fontSize: 16
      },
      legend: {
        position: "bottom"
      }
    };
  }

  getTeacherCount() {
    var teacherArr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    var studentArr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

    this.adminService.getTeacherCount().subscribe(
      (data: any) => {
        console.log("this is the teacher count", data);
        this.teacherCounter = data.data.length;

        for (var t = 0; t < data.data.length; t++) {
          console.log("month", new Date(data.data[t].createdAt).getMonth());
          teacherArr[new Date(data.data[t].createdAt).getMonth()] += 1;
        }
        this.adminService.getStudentCount().subscribe(
          (sdata: any) => {
            console.log("student data", sdata);
            for (var s = 0; s < sdata.data.length; s++) {
              // console.log("month",new Date(sdata.data[t].createdAt).getMonth());
              // if()
              studentArr[new Date(sdata.data[s].createdAt).getMonth()] += 1;
            }
            this.data = {
              labels: [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
              ],
              datasets: [
                {
                  label: "Teachers",
                  data: teacherArr,
                  borderColor: "#4bc0c0"
                },
                {
                  label: "Students",
                  data: studentArr,
                  borderColor: "#565656"
                }
              ]
            };
          },
          (error: any) => {
            console.log("htisi serror", error);
          }
        );
      },
      (error: any) => {
        console.log("this is the error", error);
      }
    );
  }
}
