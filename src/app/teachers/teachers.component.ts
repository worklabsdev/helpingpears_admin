import { Component, OnInit } from '@angular/core';
import { AdminService } from '../providers/admin.service';
import { GlobalService } from '../providers/global.service';
@Component({
  selector: 'app-teachers',
  templateUrl: './teachers.component.html',
  styleUrls: ['./teachers.component.css']
})
export class TeachersComponent implements OnInit {
allTeachers:any;
teacherDp:any;
  constructor(public adminService :AdminService,public globalService:GlobalService) { }

  ngOnInit() {
    this.getAllTeachers();
    this.teacherDp =this.globalService.serverip() + 'public/teacher/dp/'

  }


  getAllTeachers() {
    this.adminService.getAllTeachers().subscribe((data:any)=>{
      console.log("this is the data",data);
      this.allTeachers =data.data;
    },(error:any)=>{
      console.log("this is the error ",error);
    })
  }


}
