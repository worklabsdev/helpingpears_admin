import { Component, OnInit } from '@angular/core';
import { GlobalService  } from '../providers/global.service';
import { AdminService } from '../providers/admin.service';
@Component({
  selector: 'app-admin-queries',
  templateUrl: './admin-queries.component.html',
  styleUrls: ['./admin-queries.component.css']
})
export class AdminQueriesComponent implements OnInit {
AdminQueryData:any;
  constructor(public globalService:GlobalService,public adminService:AdminService ) { }

  ngOnInit() {
    this.getAllAdminQueries();
  }

  getAllAdminQueries(){
    this.adminService.getAllAdminQueries().subscribe((data:any)=>{
      console.log("thisis the data",data);
      this.AdminQueryData = data.data;
    },(error:any)=>{
      console.log("this is the error",error);
    })
  }

}
