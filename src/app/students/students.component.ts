import { Component, OnInit } from '@angular/core';
import { AdminService } from '../providers/admin.service';
import { GlobalService } from '../providers/global.service';
@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {
  allStudents:any;
  studentDp:any;
    constructor(public adminService :AdminService,public globalService:GlobalService) { }

    ngOnInit() {
      this.getAllStudents();
      this.studentDp =this.globalService.serverip() + 'public/student/dp/'

    }
  getAllStudents() {
    this.adminService.getAllStudents().subscribe((data:any)=>{
      console.log("this is the data student",data);
      this.allStudents =data.data;
    },(error:any)=>{
      console.log("this is the error ",error);
    })
  }
  singlestudent(name,email) {
    console.log("this is the email ",name,email);
  }

}
