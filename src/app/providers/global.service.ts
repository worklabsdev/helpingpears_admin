import { Injectable } from "@angular/core";
import { Http, HttpModule } from "@angular/http";
import { Observable } from "rxjs";
import { Subject } from "rxjs";
@Injectable({
  providedIn: "root"
})
export class GlobalService {
  constructor() {}

  serverip() {
    // const serv = "http://localhost:3000/";
    const serv = "http://68.183.49.174:3000/";
    //  const serv = "http://159.65.145.191:3000/";
    return serv;
  }
  authenticate() {
    // return this.http.post(this.globalservice)
  }
  langsettings = {
    singleSelection: false,
    idField: "id",
    textField: "lang_text",
    selectAllText: "Select All",
    unSelectAllText: "UnSelect All",
    itemsShowLimit: 5,
    allowSearchFilter: true
  };
  languages = [
    { id: 1, lang_text: "Albanian" },
    { id: 2, lang_text: "Ancient Greek" },
    { id: 3, lang_text: "Catalan" },
    { id: 4, lang_text: "Esperanto" },
    { id: 5, lang_text: "French" },
    { id: 6, lang_text: "Galician" },
    { id: 7, lang_text: "Hebrew" },
    { id: 8, lang_text: "Hiligaynon" },
    { id: 9, lang_text: "Hungarian" },
    { id: 10, lang_text: "Icelandic" },
    { id: 11, lang_text: "Ido" },
    { id: 12, lang_text: "Irish" },
    { id: 13, lang_text: "Italian" },
    { id: 14, lang_text: "Kashubian" },
    { id: 15, lang_text: "Latin" },
    { id: 16, lang_text: "Lithuanian" },
    { id: 17, lang_text: "Lower Sorbian" },
    { id: 18, lang_text: "Mapudungun" },
    { id: 19, lang_text: "Old French" },
    { id: 20, lang_text: "Polish" },
    { id: 21, lang_text: "Portuguese" },
    { id: 22, lang_text: "Romanian" },
    { id: 23, lang_text: "Russian" },
    { id: 24, lang_text: "Spanish" },
    { id: 25, lang_text: "Upper Sorbian" }
  ];

  subjectsetting = {
    singleSelection: false,
    idField: "id",
    textField: "sub_text",
    selectAllText: "Select All",
    unSelectAllText: "UnSelect All",
    itemsShowLimit: 5,
    allowSearchFilter: true
  };

  subjects = [
    { id: 1, sub_text: "English" },
    { id: 2, sub_text: "Mathematics" },
    { id: 3, sub_text: "Physics" },
    { id: 4, sub_text: "Chemistry" },
    { id: 5, sub_text: "French" },
    { id: 6, sub_text: "Machines" },
    { id: 7, sub_text: "Power Electronics" }
  ];

  back() {
    window.history.back();
  }
}
