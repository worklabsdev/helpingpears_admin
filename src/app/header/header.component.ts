import { Component, OnInit } from '@angular/core';
import { AdminService } from '../providers/admin.service';
import { RouterModule,Router } from '@angular/router';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(public adminService:AdminService,public router:Router) { }

  ngOnInit() {
  }

logout(){
  localStorage.clear();
  this.router.navigate(['/']);
}


}
