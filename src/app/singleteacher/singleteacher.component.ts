import { Component, OnInit } from '@angular/core';
import { AdminService } from '../providers/admin.service';
import { GlobalService } from '../providers/global.service';
import { RouterModule,Router,ActivatedRoute } from '@angular/router';
import {Message} from 'primeng/components/common/api';
import {MessageService} from 'primeng/components/common/messageservice';
@Component({
  selector: 'app-singleteacher',
  templateUrl: './singleteacher.component.html',
  styleUrls: ['./singleteacher.component.css']
})
export class SingleteacherComponent implements OnInit {
  teacherData:any;
  teacherDp:any;
  name:any;
  email:any;
  docUrl:any;
  dpUrl:any;
  msgs : Message[]=[];
    constructor(public adminService :AdminService,public globalService:GlobalService,public activatedRoute:ActivatedRoute,private messageService: MessageService) {
      this.name= this.activatedRoute.snapshot.paramMap.get('name');
      this.email= this.activatedRoute.snapshot.paramMap.get('email');
      this.dpUrl =this.globalService.serverip()  + 'public/teacher/dp/';
      this.docUrl = this.globalService.serverip() + 'public/teacher/docs/';
      console.log("this is the params",this.name,this.email);
    }

    ngOnInit() {
      this.getSingleTeacher();
      this.teacherDp =this.globalService.serverip() + 'public/teacher/dp/'

    }


    getSingleTeacher() {
      this.adminService.getSingleTeacher(this.name,this.email).subscribe((data:any)=>{
        console.log("this is the data",data);
        if(data.status==200){
        this.teacherData =data.data;
        }
      },(error:any)=>{
        console.log("this is the error ",error);
      })
    }

    teacherDegreeApprove(id,status) {
      var postData = {
        status:status
      }
      console.log("this is postData",postData);
      this.adminService.teacherDegreeApprove(id,postData).subscribe((data:any)=>{
        console.log("thisi is the data",data);
        this.messageService.add({severity:'success', summary:'Degree Approved'});
        this.getSingleTeacher();
      },(error:any)=>{
        console.log("this is the error ",error);
      })
    }
    enableDisableTeacher(id,status) {
      var statusData ={
        isEnabled:status
      }
      this.adminService.enableDisableTeacher(id,statusData).subscribe((data:any)=>{
        if(status == false){
          this.getSingleTeacher();
          this.messageService.add({severity:'success', summary:'Student De-Activated'});
        }
        else {
          this.getSingleTeacher();
          this.messageService.add({severity:'success', summary:'Student Activated'});
        }
      },(error:any)=>{
        console.log("this is the error",error);
      })
    }

}
